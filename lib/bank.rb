class Bank
  attr_accessor :balance

  def initialize(amount)
    @balance = amount
  end

  def receive_deposit(amount)
    @balance += amount
  end

  def receive_withdrawal(amount)
    @balance -= amount
  end
end
