# require_relative './bank.rb'

class User
  def initialize(bank, amount)
    @bank = Bank.new(amount)
  end

  def deposit(amount)
    @bank.receive_deposit(amount)
  end

  def withdraw(amount)
    @bank.receive_withdrawal(amount)
  end
 end
