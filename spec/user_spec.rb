require 'user'
require 'bank'

describe User do
  let (:balance) { 1500 }
  let(:bank) { Bank.new(balance)}
  let(:user) { described_class.new(bank, balance) }

  it 'expects to deposit money and balance goes up with same amount' do
    user.deposit(2000)
    expect(bank.balance).to eq 3500
  end
end
