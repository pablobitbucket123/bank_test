require 'bank'

describe Bank do
  let(:initial_balance) { 1500 }
  let(:bank) { described_class.new(initial_balance) }

  # let(:user) {instance_double("Order", )}

  it 'needs to print the balance of the account' do
    expect(bank.balance).to eq 1500
  end

  it 'expects the balance to go up by same amout of deposit' do
    # allow(user).to receive(:deposit).with(2000)
    bank.receive_deposit(2000)
    expect(bank.balance).to eq 3500
  end

  it 'expects the balance to go down by same amount of withdrawal' do
    bank.receive_withdrawal(500)
    expect(bank.balance).to eq 1000
  end
end
